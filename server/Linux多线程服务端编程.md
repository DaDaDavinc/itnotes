> muduo 是陈硕大神基于Reactor模式实现的一个高性能TCP网络库。而《Linux多线程服务端编程》一书详细的介绍了muduo的整体架构设计，是学习muduo源码和设计理念的最好的书籍了。
# 基础概念
* Reactor 

是一种事件驱动模式，指一个循环过程，不断监听对应事件是否触发，事件触发时调用对应的callback进行处理。

Reactor能够解耦并发请求的服务并分发给对应的事件处理器来处理，对于并发web请求，将解耦成连接请求、处理请求、断开连接请求等，并分发给不同的处理器处理。而不同于thread-based模式的一个客户端连接对应一个线程，同一客户的所有的操作都在同一个线程完成，耦合程度太高。

* 基础架构

![[Pasted image 20210427232811.png]]
> 该架构为最简单的Reactor模式，即单Reactor单线程模式，其中Reactor负责响应连接请求（即`Acceptor`），同时负责处理IO操作请求（`read/write/compute`）。

* 主要组成部分

1. `Handle` 文件描述符，表示事件，比如socket可读可写事件、定时器事件等。
2. `Demultiplexer`，Synchronous Event Demultiplexer 同步事件多路分解器，本质上是系统调用。比如linux中的select、poll、epoll等。阻塞等待一系列的Handle中的事件到来，如果阻塞等待返回，即表示在返回的Handle中可以不阻塞的执行返回的事件类型。这个模块一般使用操作系统的select来实现。
3. `EventHandler`，事件处理器，可以根据事件的不同状态创建处理不同状态的处理器，即定义一些回调方法或者称为钩子函数，当handle上有事件发生时，回调方法便会执行，一种事件处理机制。
4. `Concrete Event Handler`，具体的事件处理器，实现了Event Handler。在回调方法中会实现具体的业务逻辑。
5. `Initiation dispatcher`，初始分发器，也是reactor角色，用于管理EventHandler，分发事件，提供了注册、删除与转发event handler的方法。当Synchronous Event Demultiplexer检测到handle上有事件发生时，便会通知initiation dispatcher调用特定的event handler的回调方法。

* 逻辑结构图

![[Pasted image 20210429000633.png]]

* Reactor时序图

![[Pasted image 20210429002206.png]]

* 执行顺序
1. 初始化InitiationDispatcher，并初始化一个Handle到EventHandler的Map。
2. 注册EventHandler到InitiationDispatcher中，每个EventHandler包含对相应Handle的引用，从而建立Handle到EventHandler的映射（Map）。
3. 调用InitiationDispatcher的handle\_events()方法以启动Event Loop。在Event Loop中，调用select()方法（Synchronous Event Demultiplexer）阻塞等待Event发生。
4. 当某个或某些Handle的Event发生后，select()方法返回，InitiationDispatcher根据返回的Handle找到注册的EventHandler，并回调该EventHandler的handle\_events()方法。
5. 在EventHandler的handle\_events()方法中还可以向InitiationDispatcher中注册新的Eventhandler，比如对AcceptorEventHandler来，当有新的client连接时，它会产生新的EventHandler以处理新的连接，并注册到InitiationDispatcher中。

* 单Reactor多线程模型

![[Pasted image 20210429002634.png]]

> 这种模型就是现在成熟的Reactor模式。但是请求进一步增加的时候，Reactor会出现瓶颈。因为Reactor既要处理IO操作请求，又要响应连接请求！为了分担Reactor的负担，所以引入了主从Reactor模型。

* 多线程多Reactor模型 

![[Pasted image 20210429002854.png]]

> 主Reactor用于响应连接请求，从Reactor用于处理IO操作请求。



